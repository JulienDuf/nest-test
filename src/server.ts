const env = require('dotenv').config();
import * as http from 'http';
import { Application } from './app/app';
import { ApplicationModule } from './app/app.module';
import { NestFactory } from '@nestjs/core';

if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = "development";
}

if (!process.env.VERSION) {
    process.env.VERSION = '0.1.1';
}

const server = Application.bootstrap().app;
const app = NestFactory.create(ApplicationModule, server);
app.init();

http.createServer(server).listen(process.env.PORT);
