import * as express from 'express';
import { HttpStatus, Middleware, NestMiddleware } from '@nestjs/common';

@Middleware()
export class PermissionsMiddleware implements NestMiddleware {

    public resolve() {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (!req.isAuthenticated()) {
                res.status(HttpStatus.UNAUTHORIZED).json({
                    success: false
                });
            } else {
                next();
            }
        };
    }
}
