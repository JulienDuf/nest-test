import * as Sequelize from 'sequelize';

export class Database {

    Sequelize: Sequelize.Sequelize;
    static instance: Database;

    static getInstance() {
        if (!Database.instance) {
            Database.instance = new Database();
        }
        return Database.instance;
    }

    private constructor() {
        this.Sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
            host: process.env.DB_HOST,
            logging: false,
            dialect: "mariadb",
            define: {
                timestamps: true,
                paranoid: true,
                updatedAt: "updated_at",
                createdAt: "created_at",
                deletedAt: "deleted_at",
                defaultScope: {
                    attributes: {
                        exclude: ["updated_at", "created_at", "deleted_at"]
                    }
                }
            }
        });
    }

    public async connect() {
        await this.Sequelize.authenticate();
    }
}
