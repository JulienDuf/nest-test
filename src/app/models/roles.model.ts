import * as Sequelize from "sequelize";
import { Database } from './database';
import { AttributesModel } from './default.model';
import { RepositoryBase } from './repository_base';
import { UsersAttributes, UsersInstance, UsersModel } from "./users.model";

let SEQUELIZE = Database.getInstance().Sequelize;

export interface RolesAttributes extends AttributesModel {
    name?: string;
    users?: UsersInstance[];
}

export interface RolesInstance extends Sequelize.Instance<RolesAttributes>, RolesAttributes {

    /* Roles-Users Association */
    addUser: Sequelize.HasManyAddAssociationMixin<UsersInstance, Number>;
    addUsers: Sequelize.HasManyAddAssociationsMixin<UsersInstance, Number>;
    createUser: Sequelize.HasManyCreateAssociationMixin<UsersAttributes, UsersInstance>;
    countUsers: Sequelize.HasManyCountAssociationsMixin;
    hasUser: Sequelize.HasManyHasAssociationMixin<UsersInstance, Number>;
    hasUsers: Sequelize.HasManyHasAssociationsMixin<UsersInstance, Number>;
    getUsers: Sequelize.HasManyGetAssociationsMixin<UsersInstance>;
    removeUser: Sequelize.HasManyRemoveAssociationMixin<UsersInstance, Number>;
    removeUsers: Sequelize.HasManyRemoveAssociationsMixin<UsersInstance, Number>;
    setUsers: Sequelize.HasManySetAssociationsMixin<UsersInstance, Number>;
}

export interface RolesModel extends Sequelize.Model<RolesInstance, RolesAttributes> {
}

export const RolesModel = <RolesModel>SEQUELIZE.define<RolesInstance, RolesAttributes>("roles", {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    "name": { type: Sequelize.STRING, allowNull: false },
    "created_at": { type: Sequelize.DATE },
    "updated_at": { type: Sequelize.DATE }
});

export class RolesRepository extends RepositoryBase<RolesAttributes, RolesInstance, RolesModel> {

    constructor() {
        super(RolesModel);
        if (!this.isInit) {
            this.init();
        }
    }

    public init() {
        this.model.addScope("include", {
            include: [{
                model: UsersModel,
                as: "users"
            }],
            attributes: {
                exclude: ["updated_at", "created_at", "deleted_at"]
            }
        }, {
            override: true
        });
    }
}
