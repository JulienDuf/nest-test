import * as Sequelize from "sequelize";
import { Database } from './database';
import { AttributesModel } from './default.model';
import { RepositoryBase } from './repository_base';
import { RolesAttributes, RolesInstance, RolesModel } from "./roles.model";
import { PermissionsAttributes, PermissionsInstance, PermissionsModel } from "./permissions.model";
import { PermissionUsersAttributes } from "./permission_users.model";

let SEQUELIZE = Database.getInstance().Sequelize;

export interface UsersAttributes extends AttributesModel {
    username?: string;
    password?: string;
    role_id?: number;
    role?: RolesInstance;
    permissions?: PermissionsInstance[];
}

export interface UsersInstance extends Sequelize.Instance<UsersAttributes>, UsersAttributes {

    /* Users-Roles Association */
    createRole: Sequelize.BelongsToCreateAssociationMixin<RolesAttributes>;
    getRole: Sequelize.BelongsToGetAssociationMixin<RolesInstance>;
    setRole: Sequelize.BelongsToSetAssociationMixin<RolesInstance, Number>;

    /* Users-Permissions Association */
    addPermission: Sequelize.BelongsToManyAddAssociationMixin<PermissionsInstance, Number, PermissionUsersAttributes>;
    addPermissions: Sequelize.BelongsToManyAddAssociationsMixin<PermissionsInstance, Number, PermissionUsersAttributes>;
    createPermission: Sequelize.BelongsToManyCreateAssociationMixin<PermissionsAttributes, PermissionsInstance, PermissionUsersAttributes>;
    countPermissions: Sequelize.BelongsToManyCountAssociationsMixin;
    hasPermission: Sequelize.BelongsToManyHasAssociationMixin<PermissionsInstance, Number>;
    hasPermissions: Sequelize.BelongsToManyHasAssociationsMixin<PermissionsInstance, Number>;
    getPermissions: Sequelize.BelongsToManyGetAssociationsMixin<PermissionsInstance>;
    removePermission: Sequelize.BelongsToManyRemoveAssociationMixin<PermissionsInstance, Number>;
    removePermissions: Sequelize.BelongsToManyRemoveAssociationsMixin<PermissionsInstance, Number>;
    setPermissions: Sequelize.BelongsToManySetAssociationsMixin<PermissionsInstance, Number, PermissionUsersAttributes>;
}

export interface UsersModel extends Sequelize.Model<UsersInstance, UsersAttributes> {
}

export const UsersModel = <UsersModel>SEQUELIZE.define<UsersInstance, UsersAttributes>("users", {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    "username": { type: Sequelize.STRING, allowNull: false },
    "password": { type: Sequelize.STRING, allowNull: false },
    "role_id": { type: Sequelize.INTEGER },
    "created_at": { type: Sequelize.DATE },
    "updated_at": { type: Sequelize.DATE }
});

export class UsersRepository extends RepositoryBase<UsersAttributes, UsersInstance, UsersModel> {

    constructor() {
        super(UsersModel);
        if (!this.isInit) {
            this.init();
        }
    }

    public init() {
        this.model.addScope("include", {
            include: [{
                model: RolesModel,
                as: "role"
            }, {
                model: PermissionsModel,
                as: "permissions"
            }],
            attributes: {
                exclude: ["updated_at", "created_at", "deleted_at"]
            }
        }, {
            override: true
        });
    }
}
