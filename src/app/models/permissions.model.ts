import * as Sequelize from "sequelize";
import { Database } from './database';
import { AttributesModel } from './default.model';
import { RepositoryBase } from './repository_base';
import { UsersAttributes, UsersInstance, UsersModel } from "./users.model";
import { PermissionUsersAttributes } from "./permission_users.model";

let SEQUELIZE = Database.getInstance().Sequelize;

export interface PermissionsAttributes extends AttributesModel {
    name?: string;
    role_id?: number;
    users?: UsersInstance[];
}

export interface PermissionsInstance extends Sequelize.Instance<PermissionsAttributes>, PermissionsAttributes {

    /* Permissions-Users Association */
    addUser: Sequelize.BelongsToManyAddAssociationMixin<UsersInstance, Number, PermissionUsersAttributes>;
    addUsers: Sequelize.BelongsToManyAddAssociationsMixin<UsersInstance, Number, PermissionUsersAttributes>;
    createUser: Sequelize.BelongsToManyCreateAssociationMixin<UsersAttributes, UsersInstance, PermissionUsersAttributes>;
    countUsers: Sequelize.BelongsToManyCountAssociationsMixin;
    hasUser: Sequelize.BelongsToManyHasAssociationMixin<UsersInstance, Number>;
    hasUsers: Sequelize.BelongsToManyHasAssociationsMixin<UsersInstance, Number>;
    getUsers: Sequelize.BelongsToManyGetAssociationsMixin<UsersInstance>;
    removeUser: Sequelize.BelongsToManyRemoveAssociationMixin<UsersInstance, Number>;
    removeUsers: Sequelize.BelongsToManyRemoveAssociationsMixin<UsersInstance, Number>;
    setUsers: Sequelize.BelongsToManySetAssociationsMixin<UsersInstance, Number, PermissionUsersAttributes>;
}

export interface PermissionsModel extends Sequelize.Model<PermissionsInstance, PermissionsAttributes> {
}

export const PermissionsModel = <PermissionsModel>SEQUELIZE.define<PermissionsInstance, PermissionsAttributes>("permissions", {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    "name": { type: Sequelize.STRING, allowNull: false },
    "role_id": { type: Sequelize.INTEGER },
    "created_at": { type: Sequelize.DATE },
    "updated_at": { type: Sequelize.DATE }
});

export class PermissionsRepository extends RepositoryBase<PermissionsAttributes, PermissionsInstance, PermissionsModel> {

    constructor() {
        super(PermissionsModel);
        if (!this.isInit) {
            this.init();
        }
    }

    public init() {
        this.model.addScope("include", {
            include: [{
                model: UsersModel,
                as: "users"
            }],
            attributes: {
                exclude: ["updated_at", "created_at", "deleted_at"]
            }
        }, {
            override: true
        });
    }
}
