import * as Sequelize from "sequelize";
import { Database } from './database';
import { AttributesModel } from './default.model';
import { RepositoryBase } from './repository_base';


let SEQUELIZE = Database.getInstance().Sequelize;

export interface PermissionUsersAttributes extends AttributesModel {
    permission_id?: number;
    user_id?: number;
}

export interface PermissionUsersInstance extends Sequelize.Instance<PermissionUsersAttributes>, PermissionUsersAttributes {
}

export interface PermissionUsersModel extends Sequelize.Model<PermissionUsersInstance, PermissionUsersAttributes> {
}

export const PermissionUsersModel = <PermissionUsersModel>SEQUELIZE.define<PermissionUsersInstance, PermissionUsersAttributes>("permission_users", {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    "permission_id": { type: Sequelize.INTEGER, allowNull: false },
    "user_id": { type: Sequelize.INTEGER, allowNull: false },
    "created_at": { type: Sequelize.DATE },
    "updated_at": { type: Sequelize.DATE }
});

export class PermissionUsersRepository extends RepositoryBase<PermissionUsersAttributes, PermissionUsersInstance, PermissionUsersModel> {

    constructor() {
        super(PermissionUsersModel);
        if (!this.isInit) {
            this.init();
        }
    }

    public init() {
        this.model.addScope("include", {
            include: [],
            attributes: {
                exclude: ["updated_at", "created_at", "deleted_at"]
            }
        }, {
            override: true
        });
    }
}
