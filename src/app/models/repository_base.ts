import * as Sequelize from "sequelize";
import { AttributesModel } from './default.model';
const datatable = require('sequelize-datatables');
import { isNullOrUndefined } from "util";

export interface RepositoryInterface<TAttributes extends AttributesModel, TInstance extends Sequelize.Instance<TAttributes>> {

    create(element: TAttributes): Promise<TInstance>;
    update(element: TAttributes): Promise<TInstance>;
    count(): Promise<number>;
    getAll(page?: number, size?: number): Promise<TInstance[]>;
    getDatatable(option?: any): Promise<TInstance[]>;
    getById(id: number, include?: boolean): Promise<TInstance>;
    find(where: Sequelize.WhereOptions, include?: boolean): Promise<TInstance>;
    findAll(where: Sequelize.WhereOptions, include?: boolean, page?: number, size?: number): Promise<TInstance[]>;
    findOne(where: Sequelize.WhereOptions, include?: boolean): Promise<TInstance>;
    remove(id: number): Promise<boolean>;
}

export class RepositoryBase<TAttributes extends AttributesModel, TInstance extends Sequelize.Instance<TAttributes>,
    Model extends Sequelize.Model<TInstance, TAttributes>> implements RepositoryInterface<TAttributes, TInstance> {

    protected model: Model;
    protected isInit: boolean;

    constructor(model: Model) {
        this.model = model;
    }

    public async create(element: TAttributes): Promise<TInstance> {

        try {
            return await this.model.create(element);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async update(element: TAttributes): Promise<TInstance> {

        try {
            let elementToUpdate = await this.model.findOne({
                where: {
                    id: element.id
                }
            });
            return await elementToUpdate.update(element);
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async count(): Promise<number> {

        try {
            return await this.model.count();
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async getAll(page?: number, size?: number): Promise<TInstance[]> {

        try {
            if (!isNullOrUndefined(page) && !isNullOrUndefined(size)) {
                return await this.model.scope('include').findAll({ limit: size, offset: size * (page - 1) });
            } else if (!isNullOrUndefined(page) && isNullOrUndefined(size)) {
                return await this.model.scope('include').findAll({ limit: 10, offset: 10 * (page - 1) });
            } else {
                return await this.model.scope('include').findAll();
            }
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async getDatatable(options: any): Promise<any> {

        try {
            return await datatable(this.model, options, {});
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async getById(id: number, include?: boolean): Promise<TInstance> {

        try {
            let where = {
                id: id
            };
            let element;
            if (include) {
                element = await this.model.scope('include').findOne({
                    where: where
                });
            } else {
                element = await this.model.findOne({
                    where: where
                });
            }
            return element;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async find(where: Sequelize.WhereOptions, include?: boolean): Promise<TInstance> {

        try {
            let element;
            if (include) {
                element = await this.model.scope('include').find({
                    where: where
                });
            } else {
                element = await this.model.find({
                    where: where
                });
            }
            return element;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async findAll(where: Sequelize.WhereOptions, include?: boolean, page?: number, size?: number): Promise<TInstance[]> {

        try {
            let elements;
            if (include) {
                if (!isNullOrUndefined(page) && !isNullOrUndefined(size)) {
                    elements = await this.model.scope('include').findAll({
                        where: where,
                        limit: size,
                        offset: size * (page - 1) });
                } else if (!isNullOrUndefined(page) && isNullOrUndefined(size)) {
                    elements = await this.model.scope('include').findAll({
                        where: where,
                        limit: 10,
                        offset: 10 * (page - 1) });
                } else {
                    elements = await this.model.scope('include').findAll({
                        where: where
                    });
                }
            } else {
                if (!isNullOrUndefined(page) && !isNullOrUndefined(size)) {
                    elements = await this.model.findAll({
                        where: where,
                        limit: size,
                        offset: size * (page - 1) });
                } else if (!isNullOrUndefined(page) && isNullOrUndefined(size)) {
                    elements = await this.model.findAll({
                        where: where,
                        limit: 10,
                        offset: 10 * (page - 1) });
                } else {
                    elements = await this.model.findAll({
                        where: where
                    });
                }
            }
            return elements;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async findOne(where: Sequelize.WhereOptions, include?: boolean): Promise<TInstance> {

        try {
            let element;
            if (include) {
                element = await this.model.scope('include').findOne({
                    where: where
                });
            } else {
                element = await this.model.findOne({
                    where: where
                });
            }
            return element;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }

    public async remove(id: number): Promise<boolean> {

        try {
            let element = await this.model.findOne({
                where: {
                    id: id
                }
            });
            if (element) {
                await element.destroy();
                return true;
            }
            return false;
        } catch (err) {
            console.log(err);
            throw err;
        }
    }
}
