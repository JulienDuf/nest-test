import { UsersModel } from "./users.model";
import { RolesModel } from "./roles.model";
import { PermissionsModel } from "./permissions.model";
import { PermissionUsersModel } from "./permission_users.model";


export let initialized: boolean = false;

export function initialize(): any {

    /* Roles-Users Association */
    RolesModel.hasMany(UsersModel, { as: 'users', foreignKey: 'role_id' });

    /* Users-Roles Association */
    UsersModel.belongsTo(RolesModel, { as: 'role', foreignKey: 'role_id' });

    /* Users-Permissions Association */
    UsersModel.belongsToMany(PermissionsModel, { as: 'permissions', through: PermissionUsersModel, foreignKey: 'user_id' });

    /* Permissions-Users Association */
    PermissionsModel.belongsToMany(UsersModel, { as: 'users', through: PermissionUsersModel, foreignKey: 'permission_id' });


    initialized = true;

    return exports;
}
