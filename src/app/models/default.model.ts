
export interface AttributesModel {

    id?: number;
    created_at?: Date;
    updated_at?: Date;
}
