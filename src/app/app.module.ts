import { Module } from '@nestjs/common';
import { InfoModule } from "./controllers/info/info.module";

@Module({
    modules: [ InfoModule ],
    controllers: [ ]
})
export class ApplicationModule {}
