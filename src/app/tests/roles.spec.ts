const env = require('dotenv').config();
import { expect } from 'chai';
import { Test } from '@nestjs/testing';
import { Database } from '../models/database';
import { initialize } from '../models/sequelize.models';
import { RolesController } from '../controllers/roles/roles.controller';
import { RolesService } from '../services/roles.service';

describe('role service tests', () => {

    before(() => {
        initialize();
        return Database.getInstance().connect();
    });

    Test.createTestingModule({
        controllers: [ RolesController ],
        components: [ RolesService ]
    });

    const service = Test.get<RolesService>(RolesService);
    let id;
    let userInstances = [];

    it('Should pass if role creation succeed', () => {
        return service.create({
            name: "transmitting"
        }).then(role => {
            expect(role).to.not.equal(null);
            id = role.id;
        });
    });

    it('Should pass if getting role by id succeed', () => {
        return service.getById(id).then(role => {
            expect(role).to.not.equal(null);
        });
    });

    it('Should pass if getting all roles succeed', () => {
        return service.getAll().then(roles => {
            expect(roles).to.not.equal(null);
        });
    });

    it('Should pass if updating role by id succeed', () => {
        return service.update({
            id: id,
            name: "blue"
        }).then(role => {
            expect(role).to.not.equal(null);
            expect(role.name).to.equal("blue");
        });
    });

    it('Should pass if user creation succeed', () => {
        service.createUser(id, {
            username: "Group",
            password: "solution"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "Kids",
            password: "Vatu"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "auxiliary",
            password: "deposit"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "architectures",
            password: "Unbranded Wooden Soap"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "turquoise",
            password: "index"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        return service.createUser(id, {
            username: "Tasty Soft Hat",
            password: "Health"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
    });

    it('Should pass if removing one user succeed', () => {
        return service.removeUser(id, userInstances[5]).then(role => {
            expect(role).to.not.equal(null);
            expect(role.users.length).to.equal(userInstances.length - 1);
        });
    });

    it('Should pass if removing multiple users succeed', () => {
        return service.removeUsers(id, userInstances.slice(0, 6)).then(role => {
            expect(role).to.not.equal(null);
            expect(role.users.length).to.equal(0);
        });
    });

    it('Should pass if adding one user succeed', () => {
        return service.addUser(id, userInstances[4]).then(role => {
            expect(role).to.not.equal(null);
            expect(role.users.length).to.equal(1);
        });
    });

    it('Should pass if adding multiple users succeed', () => {
        return service.addUsers(id, userInstances.slice(0, 2)).then(role => {
            expect(role).to.not.equal(null);
            expect(role.users.length).to.equal(3);
        });
    });

    it('Should pass if setting users succeed', () => {
        return service.setUsers(id, userInstances.slice(3, 6)).then(role => {
            expect(role).to.not.equal(null);
            expect(role.users.length).to.equal(3);
        });
    });

    it('Should pass if having one user succeed', () => {
        return service.hasUser(id, userInstances[5]).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if having multiple users succeed', () => {
        return service.hasUsers(id, userInstances.slice(3, 6)).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if counting user succeed', () => {
        return service.countUsers(id).then(count => {
            expect(count).to.equal(3);
        });
    });

    it('Should pass if removing role by id succeed', () => {
        return service.remove(id).then(success => {
            expect(success).to.equal(true);
            return service.getById(id).then(role => {
                expect(role).to.equal(null);
            }).catch(err => {
                expect(err).to.not.equal(null);
            });
        });
    });
});
