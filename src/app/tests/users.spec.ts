const env = require('dotenv').config();
import { expect } from 'chai';
import { Test } from '@nestjs/testing';
import { Database } from '../models/database';
import { initialize } from '../models/sequelize.models';
import { UsersController } from '../controllers/users/users.controller';
import { UsersService } from '../services/users.service';

describe('user service tests', () => {

    before(() => {
        initialize();
        return Database.getInstance().connect();
    });

    Test.createTestingModule({
        controllers: [ UsersController ],
        components: [ UsersService ]
    });

    const service = Test.get<UsersService>(UsersService);
    let id;
    let roleInstance;
    let permissionInstances = [];

    it('Should pass if user creation succeed', () => {
        return service.create({
            username: "Stravenue",
            password: "Checking Account"
        }).then(user => {
            expect(user).to.not.equal(null);
            id = user.id;
        });
    });

    it('Should pass if getting user by id succeed', () => {
        return service.getById(id).then(user => {
            expect(user).to.not.equal(null);
        });
    });

    it('Should pass if getting all users succeed', () => {
        return service.getAll().then(users => {
            expect(users).to.not.equal(null);
        });
    });

    it('Should pass if updating user by id succeed', () => {
        return service.update({
            id: id,
            username: "reintermediate",
            password: "implementation"
        }).then(user => {
            expect(user).to.not.equal(null);
            expect(user.username).to.equal("reintermediate");
            expect(user.password).to.equal("implementation");
        });
    });

    it('Should pass if role creation succeed', () => {
        return service.createRole(id, {
            name: "Home Loan Account"
        }).then(role => {
            expect(role).to.not.equal(null);
            roleInstance = role;
            return service.createRole(id, {
                name: "focus group"
            }).then(role => {
                expect(role).to.not.equal(null);
            });
        });
    });

    it('Should pass if getting role by user id succeed', () => {
        return service.getRole(id).then(role => {
            expect(role).to.not.equal(null);
        });
    });

    it('Should pass if setting role by user id succeed', () => {
        return service.setRole(id, roleInstance).then(user => {
            expect(user).to.not.equal(null);
            expect(user.role).to.not.equal(null);
            expect(user.role.id).to.equal(roleInstance.id);
        });
    });

    it('Should pass if permission creation succeed', () => {
        service.createPermission(id, {
            name: "Antarctica (the territory South of 60 deg S)"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
        service.createPermission(id, {
            name: "Village"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
        service.createPermission(id, {
            name: "Principal"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
        service.createPermission(id, {
            name: "withdrawal"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
        service.createPermission(id, {
            name: "calculate"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
        return service.createPermission(id, {
            name: "Associate"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            permissionInstances.push(permission);
        });
    });

    it('Should pass if removing one permission succeed', () => {
        return service.removePermission(id, permissionInstances[5]).then(user => {
            expect(user).to.not.equal(null);
            expect(user.permissions.length).to.equal(permissionInstances.length - 1);
        });
    });

    it('Should pass if removing multiple permissions succeed', () => {
        return service.removePermissions(id, permissionInstances.slice(0, 6)).then(user => {
            expect(user).to.not.equal(null);
            expect(user.permissions.length).to.equal(0);
        });
    });

    it('Should pass if adding one permission succeed', () => {
        return service.addPermission(id, permissionInstances[4]).then(user => {
            expect(user).to.not.equal(null);
            expect(user.permissions.length).to.equal(1);
        });
    });

    it('Should pass if adding multiple permissions succeed', () => {
        return service.addPermissions(id, permissionInstances.slice(0, 2)).then(user => {
            expect(user).to.not.equal(null);
            expect(user.permissions.length).to.equal(3);
        });
    });

    it('Should pass if setting permissions succeed', () => {
        return service.setPermissions(id, permissionInstances.slice(3, 6)).then(user => {
            expect(user).to.not.equal(null);
            expect(user.permissions.length).to.equal(3);
        });
    });

    it('Should pass if having one permission succeed', () => {
        return service.hasPermission(id, permissionInstances[5]).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if having multiple permissions succeed', () => {
        return service.hasPermissions(id, permissionInstances.slice(3, 6)).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if counting permission succeed', () => {
        return service.countPermissions(id).then(count => {
            expect(count).to.equal(3);
        });
    });

    it('Should pass if removing user by id succeed', () => {
        return service.remove(id).then(success => {
            expect(success).to.equal(true);
            return service.getById(id).then(user => {
                expect(user).to.equal(null);
            }).catch(err => {
                expect(err).to.not.equal(null);
            });
        });
    });
});
