const env = require('dotenv').config();
import { expect } from 'chai';
import { Test } from '@nestjs/testing';
import { Database } from '../models/database';
import { initialize } from '../models/sequelize.models';
import { PermissionsController } from '../controllers/permissions/permissions.controller';
import { PermissionsService } from '../services/permissions.service';

describe('permission service tests', () => {

    before(() => {
        initialize();
        return Database.getInstance().connect();
    });

    Test.createTestingModule({
        controllers: [ PermissionsController ],
        components: [ PermissionsService ]
    });

    const service = Test.get<PermissionsService>(PermissionsService);
    let id;
    let userInstances = [];

    it('Should pass if permission creation succeed', () => {
        return service.create({
            name: "neural"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            id = permission.id;
        });
    });

    it('Should pass if getting permission by id succeed', () => {
        return service.getById(id).then(permission => {
            expect(permission).to.not.equal(null);
        });
    });

    it('Should pass if getting all permissions succeed', () => {
        return service.getAll().then(permissions => {
            expect(permissions).to.not.equal(null);
        });
    });

    it('Should pass if updating permission by id succeed', () => {
        return service.update({
            id: id,
            name: "Brook"
        }).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.name).to.equal("Brook");
        });
    });

    it('Should pass if user creation succeed', () => {
        service.createUser(id, {
            username: "moderator",
            password: "Bike"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "Ameliorated",
            password: "transmit"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "Horizontal",
            password: "Guinea Franc"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "invoice",
            password: "Dominican Republic"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        service.createUser(id, {
            username: "violet",
            password: "JSON"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
        return service.createUser(id, {
            username: "navigating",
            password: "synthesizing"
        }).then(user => {
            expect(user).to.not.equal(null);
            userInstances.push(user);
        });
    });

    it('Should pass if removing one user succeed', () => {
        return service.removeUser(id, userInstances[5]).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.users.length).to.equal(userInstances.length - 1);
        });
    });

    it('Should pass if removing multiple users succeed', () => {
        return service.removeUsers(id, userInstances.slice(0, 6)).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.users.length).to.equal(0);
        });
    });

    it('Should pass if adding one user succeed', () => {
        return service.addUser(id, userInstances[4]).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.users.length).to.equal(1);
        });
    });

    it('Should pass if adding multiple users succeed', () => {
        return service.addUsers(id, userInstances.slice(0, 2)).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.users.length).to.equal(3);
        });
    });

    it('Should pass if setting users succeed', () => {
        return service.setUsers(id, userInstances.slice(3, 6)).then(permission => {
            expect(permission).to.not.equal(null);
            expect(permission.users.length).to.equal(3);
        });
    });

    it('Should pass if having one user succeed', () => {
        return service.hasUser(id, userInstances[5]).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if having multiple users succeed', () => {
        return service.hasUsers(id, userInstances.slice(3, 6)).then(succeed => {
            expect(succeed).to.equal(true);
        });
    });

    it('Should pass if counting user succeed', () => {
        return service.countUsers(id).then(count => {
            expect(count).to.equal(3);
        });
    });

    it('Should pass if removing permission by id succeed', () => {
        return service.remove(id).then(success => {
            expect(success).to.equal(true);
            return service.getById(id).then(permission => {
                expect(permission).to.equal(null);
            }).catch(err => {
                expect(err).to.not.equal(null);
            });
        });
    });
});
