const env = require('dotenv').config();
import { expect } from 'chai';
import { Database } from '../models/database';

describe('Database tests', () => {
    it('Should pass if connection succeed', () => {
        return Database.getInstance().connect().catch(err => {
            expect(err).to.equal(null);
        });
    });
});
