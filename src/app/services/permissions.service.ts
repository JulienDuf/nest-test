import * as utils from './utils.service';
import { isNullOrUndefined } from 'util';
import { WhereOptions } from 'sequelize';
import { Component, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { ServiceBase } from './service_base';
import { PermissionsAttributes, PermissionsInstance, PermissionsRepository } from "../models/permissions.model";
import { UsersAttributes, UsersInstance } from "../models/users.model";
import { PermissionUsersAttributes } from "../models/permission_users.model";

@Component()
export class PermissionsService extends ServiceBase<PermissionsAttributes, PermissionsInstance> {

    constructor() {
        super("permissions", new PermissionsRepository());
    }

	/////////////////////////////////////////////////////
	//                                                 //
	//                    GENERATED                    //
	//                                                 //
	/////////////////////////////////////////////////////

	/* BelongsToMany */

    async addUser(permissionId: number, user: UsersInstance | number,
                     permissionUserAtt?: PermissionUsersAttributes): Promise<PermissionsInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            await permission.addUser(user, { through: permissionUserAtt });
            return await permission.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async addUsers(permissionId: number, users: (UsersInstance | number)[],
                      permissionUsersAtt?: PermissionUsersAttributes): Promise<PermissionsInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            await permission.addUsers(users, { through: permissionUsersAtt });
            return await permission.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async createUser(permissionId: number, userAtt: UsersAttributes,
                        permissionUserAtt?: PermissionUsersAttributes): Promise<UsersInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            return await permission.createUser(userAtt, permissionUserAtt);
        } catch (err) {
            utils.error(err);
        }
    }

    async countUsers(permissionId: number, where?: WhereOptions): Promise<number> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            let count = await permission.countUsers(where);
            if (isNullOrUndefined(count)) {
                throw new HttpException("Failed to count permission's users\n" +
                    "permission: " + JSON.stringify(permission.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return count;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasUser(permissionId: number, user: UsersInstance | number, where?: WhereOptions): Promise<boolean> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            let hasUser: boolean = await permission.hasUser(user, where);
            if (isNullOrUndefined(hasUser)) {
                throw new HttpException("Failed to check if permission has user\n" +
                    "permission: " + JSON.stringify(permission.toJSON()) + "\n" +
                    "user: " + JSON.stringify(user) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasUser;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasUsers(permissionId: number, users: (UsersInstance | number)[], where?: WhereOptions): Promise<boolean> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            let hasUsers: boolean = await permission.hasUsers(users, where);
            if (isNullOrUndefined(hasUsers)) {
                throw new HttpException("Failed to check if permission has users\n" +
                    "permission: " + JSON.stringify(permission.toJSON()) + "\n" +
                    "users: " + JSON.stringify(users) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasUsers;
        } catch (err) {
            utils.error(err);
        }
    }

    async getUsers(permissionId: number, where?: WhereOptions): Promise<UsersInstance[]> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            let users: UsersInstance[] = await permission.getUsers(where);
            if (isNullOrUndefined(users)) {
                throw new HttpException("Failed to get permission's users\n" +
                    "permission: " + JSON.stringify(permission.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return users;
        } catch (err) {
            utils.error(err);
        }
    }

    async removeUser(permissionId: number, user: UsersInstance | number): Promise<PermissionsInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            await permission.removeUser(user);
            return await permission.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async removeUsers(permissionId: number, users: (UsersInstance | number)[]): Promise<PermissionsInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            await permission.removeUsers(users);
            return await permission.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async setUsers(permissionId: number, users: (UsersInstance | number)[],
                      permissionUsersAtt?: PermissionUsersAttributes): Promise<PermissionsInstance> {
        try {
            let permission: PermissionsInstance = await this.getById(permissionId);
            await permission.setUsers(users, { through: permissionUsersAtt });
            return await permission.reload();
        } catch (err) {
            utils.error(err);
        }
    }
}
