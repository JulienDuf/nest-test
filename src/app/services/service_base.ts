import * as Sequelize from "sequelize";
import { Component, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { RepositoryInterface } from "../models/repository_base";

@Component()
export class ServiceBase<TAttributes, TInstance extends Sequelize.Instance<TAttributes>> {

    protected name: string;
    protected repository: RepositoryInterface<TAttributes, TInstance>;

    constructor(name: string, repository) {
        this.name = name;
        this.repository = repository;
    }

    async count(): Promise<number> {
        try {
            return await this.repository.count();
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getAll(page?: number, size?: number): Promise<TInstance[]> {
        let doc: TInstance[];
        try {
            doc = await this.repository.getAll(page, size);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to get " + this.name, HttpStatus.BAD_REQUEST);
        }
    }

    async getDatatable(option?: any): Promise<any> {
        let doc: any;
        try {
            doc = await this.repository.getDatatable(option);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to get " + this.name, HttpStatus.BAD_REQUEST);
        }
    }

    async getById(id: number): Promise<TInstance> {
        let doc: TInstance;
        try {
            doc = await this.repository.getById(id, true);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to get " + this.name + " for id " + id, HttpStatus.BAD_REQUEST);
        }
    }

    async findOne(where): Promise<TInstance> {
        let doc: TInstance;
        try {
            doc = await this.repository.findOne(where, true);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to find " + this.name + " where " + where.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    async findAll(where, include?: boolean, page?: number, size?: number): Promise<TInstance[]> {
        let doc: TInstance[];
        try {
            doc = await this.repository.findAll(where, include, page, size);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to find " + this.name + " where " + where.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    async create(object: TAttributes): Promise<TInstance> {
        let doc: TInstance;
        try {
            doc = await this.repository.create(object);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to create " + this.name + " for " + object.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    async update(object: TAttributes): Promise<TInstance> {
        let doc: TInstance;
        try {
            doc = await this.repository.update(object);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to update " + this.name + " for " + object.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    async remove(id: number): Promise<boolean> {
        let doc: boolean;
        try {
            doc = await this.repository.remove(id);
        } catch (err) {
            console.log(err);
            throw new HttpException("Server error\n\t" + err.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (doc) {
            return doc;
        } else {
            throw new HttpException("Failed to remove " + this.name + " for " + id, HttpStatus.BAD_REQUEST);
        }
    }
}
