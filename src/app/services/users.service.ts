import * as utils from './utils.service';
import { isNullOrUndefined } from 'util';
import { WhereOptions } from 'sequelize';
import { Component, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { ServiceBase } from './service_base';
import { UsersAttributes, UsersInstance, UsersRepository } from "../models/users.model";
import { RolesAttributes, RolesInstance } from "../models/roles.model";
import { PermissionsAttributes, PermissionsInstance } from "../models/permissions.model";
import { PermissionUsersAttributes } from "../models/permission_users.model";

@Component()
export class UsersService extends ServiceBase<UsersAttributes, UsersInstance> {

    constructor() {
        super("users", new UsersRepository());
    }

	/////////////////////////////////////////////////////
	//                                                 //
	//                    GENERATED                    //
	//                                                 //
	/////////////////////////////////////////////////////

	/* BelongsTo */

    async createRole(userId: number, roleAtt: RolesAttributes): Promise<RolesInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.createRole(roleAtt);
            let role: RolesInstance = await user.getRole();
            if (isNullOrUndefined(role)) {
                throw new HttpException("Failed to create user's role\n" +
                    "user: " + JSON.stringify(user.toJSON()) + "\n" +
                    "role: " + JSON.stringify(roleAtt), HttpStatus.BAD_REQUEST);
            }
            return role;
        } catch (err) {
            utils.error(err);
        }
    }

    async getRole(userId: number): Promise<RolesInstance> {
        try {
            let user: UsersInstance = await this.repository.getById(userId);
            if (isNullOrUndefined(user)) {
                throw new HttpException("Failed to get user\n" + "userId: " + userId, HttpStatus.BAD_REQUEST);
            }
            let role: RolesInstance = await user.getRole();
            if (isNullOrUndefined(role)) {
                throw new HttpException("Failed to get user's role\n" +
                    "user: " + JSON.stringify(user.toJSON()), HttpStatus.BAD_REQUEST);
            }
            return role;
        } catch (err) {
            utils.error(err);
        }
    }

    async setRole(userId: number, role: RolesInstance | number): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.repository.getById(userId);
            if (isNullOrUndefined(user)) {
                throw new HttpException("Failed to get user\n" + "userId: " + userId, HttpStatus.BAD_REQUEST);
            }
            await user.setRole(role);
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }

	/* BelongsToMany */

    async addPermission(userId: number, permission: PermissionsInstance | number,
                     permissionUserAtt?: PermissionUsersAttributes): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.addPermission(permission, { through: permissionUserAtt });
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }

    async addPermissions(userId: number, permissions: (PermissionsInstance | number)[],
                      permissionUsersAtt?: PermissionUsersAttributes): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.addPermissions(permissions, { through: permissionUsersAtt });
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }

    async createPermission(userId: number, permissionAtt: PermissionsAttributes,
                        permissionUserAtt?: PermissionUsersAttributes): Promise<PermissionsInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            return await user.createPermission(permissionAtt, permissionUserAtt);
        } catch (err) {
            utils.error(err);
        }
    }

    async countPermissions(userId: number, where?: WhereOptions): Promise<number> {
        try {
            let user: UsersInstance = await this.getById(userId);
            let count = await user.countPermissions(where);
            if (isNullOrUndefined(count)) {
                throw new HttpException("Failed to count user's permissions\n" +
                    "user: " + JSON.stringify(user.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return count;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasPermission(userId: number, permission: PermissionsInstance | number, where?: WhereOptions): Promise<boolean> {
        try {
            let user: UsersInstance = await this.getById(userId);
            let hasPermission: boolean = await user.hasPermission(permission, where);
            if (isNullOrUndefined(hasPermission)) {
                throw new HttpException("Failed to check if user has permission\n" +
                    "user: " + JSON.stringify(user.toJSON()) + "\n" +
                    "permission: " + JSON.stringify(permission) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasPermission;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasPermissions(userId: number, permissions: (PermissionsInstance | number)[], where?: WhereOptions): Promise<boolean> {
        try {
            let user: UsersInstance = await this.getById(userId);
            let hasPermissions: boolean = await user.hasPermissions(permissions, where);
            if (isNullOrUndefined(hasPermissions)) {
                throw new HttpException("Failed to check if user has permissions\n" +
                    "user: " + JSON.stringify(user.toJSON()) + "\n" +
                    "permissions: " + JSON.stringify(permissions) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasPermissions;
        } catch (err) {
            utils.error(err);
        }
    }

    async getPermissions(userId: number, where?: WhereOptions): Promise<PermissionsInstance[]> {
        try {
            let user: UsersInstance = await this.getById(userId);
            let permissions: PermissionsInstance[] = await user.getPermissions(where);
            if (isNullOrUndefined(permissions)) {
                throw new HttpException("Failed to get user's permissions\n" +
                    "user: " + JSON.stringify(user.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return permissions;
        } catch (err) {
            utils.error(err);
        }
    }

    async removePermission(userId: number, permission: PermissionsInstance | number): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.removePermission(permission);
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }

    async removePermissions(userId: number, permissions: (PermissionsInstance | number)[]): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.removePermissions(permissions);
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }

    async setPermissions(userId: number, permissions: (PermissionsInstance | number)[],
                      permissionUsersAtt?: PermissionUsersAttributes): Promise<UsersInstance> {
        try {
            let user: UsersInstance = await this.getById(userId);
            await user.setPermissions(permissions, { through: permissionUsersAtt });
            return await this.repository.getById(userId, true);
        } catch (err) {
            utils.error(err);
        }
    }
}
