import * as utils from './utils.service';
import { isNullOrUndefined } from 'util';
import { WhereOptions } from 'sequelize';
import { Component, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { ServiceBase } from './service_base';
import { RolesAttributes, RolesInstance, RolesRepository } from "../models/roles.model";
import { UsersAttributes, UsersInstance } from "../models/users.model";

@Component()
export class RolesService extends ServiceBase<RolesAttributes, RolesInstance> {

    constructor() {
        super("roles", new RolesRepository());
    }

	/////////////////////////////////////////////////////
	//                                                 //
	//                    GENERATED                    //
	//                                                 //
	/////////////////////////////////////////////////////

	/* HasMany */

    async addUser(roleId: number, user: UsersInstance | number): Promise<RolesInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            await role.addUser(user);
            return await role.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async addUsers(roleId: number, users: (UsersInstance | number)[]): Promise<RolesInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            await role.addUsers(users);
            return await role.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async createUser(roleId: number, userAtt: UsersAttributes): Promise<UsersInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            return await role.createUser(userAtt);
        } catch (err) {
            utils.error(err);
        }
    }

    async countUsers(roleId: number, where?: WhereOptions): Promise<number> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            let count = await role.countUsers(where);
            if (isNullOrUndefined(count)) {
                throw new HttpException("Failed to count role's users\n" +
                    "role: " + JSON.stringify(role.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return count;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasUser(roleId: number, user: UsersInstance | number, where?: WhereOptions): Promise<boolean> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            let hasUser: boolean = await role.hasUser(user, where);
            if (isNullOrUndefined(hasUser)) {
                throw new HttpException("Failed to check if role has user\n" +
                    "role: " + JSON.stringify(role.toJSON()) + "\n" +
                    "user: " + JSON.stringify(user) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasUser;
        } catch (err) {
            utils.error(err);
        }
    }

    async hasUsers(roleId: number, users: (UsersInstance | number)[], where?: WhereOptions): Promise<boolean> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            let hasUsers: boolean = await role.hasUsers(users);
            if (isNullOrUndefined(hasUsers)) {
                throw new HttpException("Failed to check if role has users\n" +
                    "role: " + JSON.stringify(role.toJSON()) + "\n" +
                    "users: " + JSON.stringify(users) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return hasUsers;
        } catch (err) {
            utils.error(err);
        }
    }

    async getUsers(roleId: number, where?: WhereOptions): Promise<UsersInstance[]> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            let users: UsersInstance[] = await role.getUsers(where);
            if (isNullOrUndefined(users)) {
                throw new HttpException("Failed to get role's users\n" +
                    "role: " + JSON.stringify(role.toJSON()) + "\n" +
                    "where: " + isNullOrUndefined(where) ? "undefined" : JSON.stringify(where), HttpStatus.BAD_REQUEST);
            }
            return users;
        } catch (err) {
            utils.error(err);
        }
    }

    async removeUser(roleId: number, user: UsersInstance | number): Promise<RolesInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            await role.removeUser(user);
            return await role.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async removeUsers(roleId: number, users: (UsersInstance | number)[]): Promise<RolesInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            await role.removeUsers(users);
            return await role.reload();
        } catch (err) {
            utils.error(err);
        }
    }

    async setUsers(roleId: number, users: (UsersInstance | number)[]): Promise<RolesInstance> {
        try {
            let role: RolesInstance = await this.getById(roleId);
            await role.setUsers(users);
            return await role.reload();
        } catch (err) {
            utils.error(err);
        }
    }
}
