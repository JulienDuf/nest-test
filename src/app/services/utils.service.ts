import { HttpException } from "@nestjs/core";
import { HttpStatus } from "@nestjs/common";

export function error(err) {
    if (err instanceof HttpException) {
        console.log(err.getResponse());
        throw err;
    } else {
        let msg = "Server error \n" + JSON.stringify(err);
        console.log(msg);
        throw new HttpException(msg, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
