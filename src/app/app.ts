import * as express from 'express';
import * as logger from 'morgan';
import * as session from 'express-session';
import * as passport from 'passport';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { Database } from './models/database';
import { initialize } from './models/sequelize.models';

export class Application {

    public app: express.Application;

    public static bootstrap(): Application {
        return new Application();
    }

    constructor() {
        this.app = express();
        this.config();
        this.database().catch((err) => { console.log(err); process.exit(-1); });
    }

    private config() {
        this.app.use(logger('dev'));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(session({
            secret: '42138416-f6ea-4254-b312-e5e3e4585ddb',
            resave: true,
            rolling: true,
            saveUninitialized: false,
            cookie: {
                maxAge: 60000 * 60 * 4
            }
        }));
        this.app.use(passport.initialize());
        this.app.use(passport.session());
        this.app.use(cors({
            preflightContinue: true
        }));
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (req.headers["origin"]) {
                res.setHeader("Access-Control-Allow-Origin", req.headers["origin"]);
                res.setHeader("Access-Control-Allow-Credentials", "true");
            }
            next();
        });
    }

    private async database() {
        await Database.getInstance().connect();
        initialize();
    }
}
