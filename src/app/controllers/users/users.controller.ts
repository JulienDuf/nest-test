import * as express from 'express';
import * as utils from '../utils';
import { isUndefined } from 'util';
import { Controller, Response, Get, Post, Patch, Request, Param, Delete, Put, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { UsersService } from '../../services/users.service';
import { UsersAttributes } from '../../models/users.model';
import { RolesController } from "../roles/roles.controller";
import { PermissionsController } from "../permissions/permissions.controller";

@Controller('user')
export class UsersController {

    public static validateRequest(req: express.Request) {
        let username = req.body["username"];
        let password = req.body["password"];

        if (isUndefined(username) || isUndefined(password)) {
            throw new HttpException("Missing one or more parameter", HttpStatus.PRECONDITION_FAILED);
        } else {
            return <UsersAttributes> {
                username: username,
                password: password,
                role_id: req.body["role_id"]
            };
        }
    }

    constructor(private userService: UsersService) { }

    @Post()
    public async create(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            let userAtt = UsersController.validateRequest(req);
            let user = await this.userService.create(userAtt);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post('datatable')
    async getDataTable(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            res.json(await this.userService.getDatatable(req.body));
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get()
    public async getAll(@Response() res: express.Response) {
        try {
            let users = await this.userService.getAll();
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id')
    public async getById(@Response() res: express.Response, @Param('id') id) {
        try {
            let user = await this.userService.getById(id);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id')
    public async update(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {

            let newUser: UsersAttributes = {
                id: id
            };

            if (req.body["username"]) {
                newUser.username = req.body["username"];
            }
            if (req.body["password"]) {
                newUser.password = req.body["password"];
            }
            if (req.body["role_id"]) {
                newUser.role_id = req.body["role_id"];
            }

            let user = await this.userService.update(newUser);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id')
    public async remove(@Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.userService.remove(id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post(':id/role')
    public async createRole(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let roleAtt = RolesController.validateRequest(req);
            let role = await this.userService.createRole(id, roleAtt);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id/role')
    public async getRole(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let role = await this.userService.getRole(id);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Patch(':id/role/:role_id')
    public async setRole(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('role_id') role_id) {
        try {
            let role = await this.userService.setRole(id, role_id);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post(':id/permission')
    public async createPermission(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let permissionAtt = PermissionsController.validateRequest(req);
            let permission = await this.userService.createPermission(id, permissionAtt);
            res.json({
                success: true,
                permission: permission
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id/permission')
    public async getPermissions(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let permissions = await this.userService.getPermissions(id);
            res.json({
                success: true,
                permissions: permissions
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/permission')
    public async addPermissions(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let permissions = await this.userService.addPermissions(id, req.body["permissions"]);
            res.json({
                success: true,
                permissions: permissions
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/permission/:permission_id')
    public async addPermission(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('permission_id') permission_id) {
        try {
            let permission = await this.userService.addPermission(id, permission_id);
            res.json({
                success: true,
                permission: permission
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Patch(':id/permission')
    public async setPermissions(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let permissions = await this.userService.setPermissions(id, req.body["permissions"]);
            res.json({
                success: true,
                permissions: permissions
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/permission')
    public async removePermissions(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.userService.removePermissions(id, req.body["permissions"])
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/permission/:permission_id')
    public async removePermission(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('permission_id') permission_id) {
        try {
            res.json({
                success: await this.userService.removePermission(id, permission_id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }
}
