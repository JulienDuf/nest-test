import { Module, NestModule, MiddlewaresConsumer } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from '../../services/users.service';
import { PermissionsMiddleware } from '../../middlewares/permissions.middleware';

@Module({
    controllers: [ UsersController ],
    components: [ UsersService ]
})
export class UsersModule implements NestModule {
    public configure(consumer: MiddlewaresConsumer) {
        consumer.apply(PermissionsMiddleware).forRoutes(UsersController);
    }
}
