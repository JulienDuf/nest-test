import * as express from "express";
import { HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';

export function error(res: express.Response, err) {
    if (err instanceof HttpException) {
        res.status(err.getStatus()).json({
            success: false,
            msg: err.getResponse().toString()
        });
    } else {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
            success: false,
            msg: err.toString()
        });
    }
}
