import * as express from 'express';
import * as utils from '../utils';
import { isUndefined } from 'util';
import { Controller, Response, Get, Post, Patch, Request, Param, Delete, Put, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { RolesService } from '../../services/roles.service';
import { RolesAttributes } from '../../models/roles.model';
import { UsersController } from "../users/users.controller";

@Controller('role')
export class RolesController {

    public static validateRequest(req: express.Request) {
        let name = req.body["name"];

        if (isUndefined(name)) {
            throw new HttpException("Missing one or more parameter", HttpStatus.PRECONDITION_FAILED);
        } else {
            return <RolesAttributes> {
                name: name
            };
        }
    }

    constructor(private roleService: RolesService) { }

    @Post()
    public async create(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            let roleAtt = RolesController.validateRequest(req);
            let role = await this.roleService.create(roleAtt);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post('datatable')
    async getDataTable(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            res.json(await this.roleService.getDatatable(req.body));
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get()
    public async getAll(@Response() res: express.Response) {
        try {
            let roles = await this.roleService.getAll();
            res.json({
                success: true,
                roles: roles
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id')
    public async getById(@Response() res: express.Response, @Param('id') id) {
        try {
            let role = await this.roleService.getById(id);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id')
    public async update(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {

            let newRole: RolesAttributes = {
                id: id
            };

            if (req.body["name"]) {
                newRole.name = req.body["name"];
            }

            let role = await this.roleService.update(newRole);
            res.json({
                success: true,
                role: role
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id')
    public async remove(@Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.roleService.remove(id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post(':id/user')
    public async createUser(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let userAtt = UsersController.validateRequest(req);
            let user = await this.roleService.createUser(id, userAtt);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id/user')
    public async getUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.roleService.getUsers(id);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/user')
    public async addUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.roleService.addUsers(id, req.body["users"]);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/user/:user_id')
    public async addUser(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('user_id') user_id) {
        try {
            let user = await this.roleService.addUser(id, user_id);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Patch(':id/user')
    public async setUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.roleService.setUsers(id, req.body["users"]);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/user')
    public async removeUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.roleService.removeUsers(id, req.body["users"])
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/user/:user_id')
    public async removeUser(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('user_id') user_id) {
        try {
            res.json({
                success: await this.roleService.removeUser(id, user_id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }
}
