import { Module, NestModule, MiddlewaresConsumer } from '@nestjs/common';
import { RolesController } from './roles.controller';
import { RolesService } from '../../services/roles.service';
import { PermissionsMiddleware } from '../../middlewares/permissions.middleware';

@Module({
    controllers: [ RolesController ],
    components: [ RolesService ]
})
export class RolesModule implements NestModule {
    public configure(consumer: MiddlewaresConsumer) {
        consumer.apply(PermissionsMiddleware).forRoutes(RolesController);
    }
}
