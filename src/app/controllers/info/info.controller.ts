import * as express from 'express';
import { Controller, Response, Get } from '@nestjs/common';

@Controller('info')
export class InfoController {

    @Get()
    public async info(@Response() res: express.Response) {
        res.json({
            version: process.env.VERSION,
            run_config: process.env.NODE_ENV
        });
    }
}
