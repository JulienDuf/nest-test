import * as express from 'express';
import * as utils from '../utils';
import { isUndefined } from 'util';
import { Controller, Response, Get, Post, Patch, Request, Param, Delete, Put, HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/core';
import { PermissionsService } from '../../services/permissions.service';
import { PermissionsAttributes } from '../../models/permissions.model';
import { UsersController } from "../users/users.controller";

@Controller('permission')
export class PermissionsController {

    public static validateRequest(req: express.Request) {
        let name = req.body["name"];

        if (isUndefined(name)) {
            throw new HttpException("Missing one or more parameter", HttpStatus.PRECONDITION_FAILED);
        } else {
            return <PermissionsAttributes> {
                name: name,
                role_id: req.body["role_id"]
            };
        }
    }

    constructor(private permissionService: PermissionsService) { }

    @Post()
    public async create(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            let permissionAtt = PermissionsController.validateRequest(req);
            let permission = await this.permissionService.create(permissionAtt);
            res.json({
                success: true,
                permission: permission
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post('datatable')
    async getDataTable(@Request() req: express.Request, @Response() res: express.Response) {
        try {
            res.json(await this.permissionService.getDatatable(req.body));
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get()
    public async getAll(@Response() res: express.Response) {
        try {
            let permissions = await this.permissionService.getAll();
            res.json({
                success: true,
                permissions: permissions
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id')
    public async getById(@Response() res: express.Response, @Param('id') id) {
        try {
            let permission = await this.permissionService.getById(id);
            res.json({
                success: true,
                permission: permission
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id')
    public async update(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {

            let newPermission: PermissionsAttributes = {
                id: id
            };

            if (req.body["name"]) {
                newPermission.name = req.body["name"];
            }
            if (req.body["role_id"]) {
                newPermission.role_id = req.body["role_id"];
            }

            let permission = await this.permissionService.update(newPermission);
            res.json({
                success: true,
                permission: permission
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id')
    public async remove(@Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.permissionService.remove(id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Post(':id/user')
    public async createUser(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let userAtt = UsersController.validateRequest(req);
            let user = await this.permissionService.createUser(id, userAtt);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Get(':id/user')
    public async getUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.permissionService.getUsers(id);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/user')
    public async addUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.permissionService.addUsers(id, req.body["users"]);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Put(':id/user/:user_id')
    public async addUser(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('user_id') user_id) {
        try {
            let user = await this.permissionService.addUser(id, user_id);
            res.json({
                success: true,
                user: user
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Patch(':id/user')
    public async setUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            let users = await this.permissionService.setUsers(id, req.body["users"]);
            res.json({
                success: true,
                users: users
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/user')
    public async removeUsers(@Request() req: express.Request, @Response() res: express.Response, @Param('id') id) {
        try {
            res.json({
                success: await this.permissionService.removeUsers(id, req.body["users"])
            });
        } catch (err) {
            utils.error(res, err);
        }
    }

    @Delete(':id/user/:user_id')
    public async removeUser(@Request() req: express.Request, @Response() res: express.Response,
                                              @Param('id') id, @Param('user_id') user_id) {
        try {
            res.json({
                success: await this.permissionService.removeUser(id, user_id)
            });
        } catch (err) {
            utils.error(res, err);
        }
    }
}
