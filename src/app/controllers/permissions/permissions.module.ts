import { Module, NestModule, MiddlewaresConsumer } from '@nestjs/common';
import { PermissionsController } from './permissions.controller';
import { PermissionsService } from '../../services/permissions.service';
import { PermissionsMiddleware } from '../../middlewares/permissions.middleware';

@Module({
    controllers: [ PermissionsController ],
    components: [ PermissionsService ]
})
export class PermissionsModule implements NestModule {
    public configure(consumer: MiddlewaresConsumer) {
        consumer.apply(PermissionsMiddleware).forRoutes(PermissionsController);
    }
}
