'use strict';

/////////////////////////////////////////////////////
//                                                 //
//                    GENERATED                    //
//                                                 //
/////////////////////////////////////////////////////

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable("users", {
            "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            "username": { type: Sequelize.STRING, allowNull: false },
            "password": { type: Sequelize.STRING, allowNull: false },
            "role_id": {
                type: Sequelize.INTEGER,
                references: {
                    model: "roles",
                    key: "id"
                },
                onUpdate: "cascade",
                onDelete: "cascade"
            },
            "created_at": { type: Sequelize.DATE },
            "updated_at": { type: Sequelize.DATE },
            "deleted_at": { type: Sequelize.DATE }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("users");
    }
};
