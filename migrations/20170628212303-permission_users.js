'use strict';

/////////////////////////////////////////////////////
//                                                 //
//                    GENERATED                    //
//                                                 //
/////////////////////////////////////////////////////

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable("permission_users", {
            "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
            "permission_id": {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: "permissions",
                    key: "id"
                },
                onUpdate: "cascade",
                onDelete: "cascade"
            },
            "user_id": {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: "users",
                    key: "id"
                },
                onUpdate: "cascade",
                onDelete: "cascade"
            },
            "created_at": { type: Sequelize.DATE },
            "updated_at": { type: Sequelize.DATE },
            "deleted_at": { type: Sequelize.DATE }
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable("permission_users");
    }
};
